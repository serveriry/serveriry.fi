alert("Palvelimet syttyi tuleen niin heitimme ne mereen");
var kissa = `\n
                  |\\ |\\_
                   \\\` ..\\
              __,.-" =__Y=
            ."        )
      _    /   ,    \\/\\_
     ((____|    )_-\\ \\_-\`
     \`-----'\`-----\` \`--\`\``

console.error(kissa)
console.error("Oon toi kissa!")
var canvas = document.getElementById('bgCanvas');
var ctx = canvas.getContext('2d');
var particles = [];
var particleCount = 300;
var isLooping = false;
var forceStop = false

for(var i=0; i<particleCount;i++)
  particles.push(new particle());

function refresh() {
  canvas = document.getElementById('bgCanvas');
  ctx = canvas.getContext('2d');
}

function particle() {
  this.x = Math.random() * canvas.width;
  this.y = canvas.height + Math.random() * 300;
  this.speed = 0.1 + Math.random()/2;
  this.radius = Math.random() * 3;
  this.opacity = (Math.random() * 100) / 1000;
}


function loop() {
  if(!forceStop) {
    isLooping = true;
    requestAnimationFrame(loop);
    draw();
  }
}

function draw() {
  ctx.clearRect(0,0,canvas.width,canvas.height);
  
  ctx.globalCompositeOperation = 'lighter';
  for(var i=0; i<particles.length; i++) {
    var p = particles[i];
    ctx.beginPath();
    ctx.fillStyle = 'rgba(255,255,255,' + p.opacity + ')';
    ctx.arc(p.x, p.y, p.radius, 0, Math.PI*2, false);
    ctx.fill();
    p.y -= p.speed;
    if(p.y <= -10)
      particles[i] = new particle();
  }
}

loop();

function looop(isCreated) {
  if(isCreated) {
    forceStop = true
    refresh()
    isLooping = true;
    forceStop = false
    loop()
  }
  else if (isLooping) {
  }
  else {
    isLooping = true;
    loop()
  }
}

function shutdown(isDestroyed) {
  forceStop = isDestroyed;
}

export default ({ app }, inject) => {
  inject("hello", isCreated => looop(isCreated))
  inject("bye", isDestroyed => shutdown(isDestroyed))
}