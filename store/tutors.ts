import TutorService from "~/services/PostsService"

export const state = () => ({
  tutors: [] as any[],
  error: null as any
})

export type TutorState = ReturnType<typeof state>

export const mutations = {
  SET_SPONSORS(state: any, responsetutors: any) {
    state.tutors = responsetutors
  },
}

export const getters = {
  // Searches for correct array index by id and returns that
  getTutorIndex: (state: any) => (id: any) => {
    let i
    for (i = 0; i <= state.tutors.length; i++) {
      console.log("beforeerror?: " + state.tutors)
      try {
        if (state.tutors[i].url == id) {
          return i
        }
      } catch {
        console.log("get tutorindex failed")
        return 0
      }

      console.log("aftererror?")
    }
    // Implement not found error here!!
  },
  getTutorId: (state: any) => (index: any) => {
    if (index <= -1 || index > state.tutors.length) {
      console.log('requested index is out of range')
      return 'requested index out of range'
    }
    else {
      console.log('requested index is in range')
      if (state.tutors[index] == 'undefined') {
        console.log("requested index doesn't exist")
        return 'id doesnt exist'
      }
      else {
        try {
          let id = state.tutors[index]
          return id
        }
        catch {
          console.log('posts/gettutorid reading state.posts[index].postid failed')
          // Handle next post missing case
          return false
        }
      }
    }

    // implement not found
  }
}

// https://www.youtube.com/watch?v=NS0io3Z75GI
export const actions = {
  async loadPosts({ commit } : {commit:any}) {
    return TutorService.getPosts().then(response => {
      commit('SET_POSTS', response.data.posts)
    })
  }
}