import PostsService from "~/services/PostsService"

export const state = () => ({
  posts: [] as any[],
  error: null as any
})

export type PostState = ReturnType<typeof state>

export const mutations = {
  SET_POSTS(state: any, responseposts: any) {
    state.posts = responseposts
  },
}

export const getters = {
  // Searches for correct array index by id and returns that
  getPostIndex: (state: any) => (id: any) => {
    let i
    for (i = 0; i <= state.posts.length; i++) {
      //console.log("beforeerror?: " + state.posts)
      try {
        if (state.posts[i].url == id) {
          return i
        }
      } catch {
        console.log("get postindex failed")
        return 0
      }

      //console.log("aftererror?")
    }
    // Implement not found error here!!
  },
  getPostId: (state: any) => (index: any) => {
    if (index <= -1 || index > state.posts.length) {
      console.log('requested index is out of range')
      return 'requested index out of range'
    }
    else {
      console.log('requested index is in range')
      if (state.posts[index] == 'undefined') {
        console.log("requested index doesn't exist")
        return 'id doesnt exist'
      }
      else {
        try {
          let id = state.posts[index]
          return id
        }
        catch {
          console.log('posts/getpostid reading state.posts[index].postid failed')
          // Handle next post missing case
          return false
        }
      }
    }

    // implement not found
  }
}

// https://www.youtube.com/watch?v=NS0io3Z75GI
export const actions = {
  async loadPosts({ commit } : {commit:any}) {
    return PostsService.getPosts().then(response => {
      commit('SET_POSTS', response.data.posts)
    })
  }
}

/*mutations: {
        SET_RESPONSE(p) {
            state.posts = p;
        }
    },
    actions: {
        async getIP ({ commit }) {
            const posts = await this.$axios.$get('localhost:9000/fi/posts')
            commit('SET_RESPONSE', posts)
        }
    },
    getters: {}
}


/*
export const state: {

    actions: {
      async getIP ({ commit }) {
        const ip = await this.$axios.$get('http://icanhazip.com')
        commit('SET_IP', ip)
      }
    }
}
*/
