export const state = () => ({
    language: "fi" as string,
    // Host will be used as a prefix in queries to api
    // Must have protocol, domain and port if non standard
    host: "https://api.serveriry.fi" as string,
    baseurl: "https://serveriry.fi" as string,
    stripekey: "pk_live_9ESpxdoR7XBmgJgwlbr5i5pa" as string
  })