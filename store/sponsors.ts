import SponsorService from "~/services/PostsService"

export const state = () => ({
  sponsors: [] as any[],
  error: null as any
})

export type SponsorState = ReturnType<typeof state>

export const mutations = {
  SET_SPONSORS(state: any, responsesponsors: any) {
    state.sponsors = responsesponsors
  },
}

export const getters = {
  // Searches for correct array index by id and returns that
  getSponsorIndex: (state: any) => (id: any) => {
    let i
    for (i = 0; i <= state.sponsors.length; i++) {
      console.log("beforeerror?: " + state.sponsors)
      try {
        if (state.sponsors[i].url == id) {
          return i
        }
      } catch {
        console.log("get sponsorindex failed")
        return 0
      }

      console.log("aftererror?")
    }
    // Implement not found error here!!
  },
  getSponsorId: (state: any) => (index: any) => {
    if (index <= -1 || index > state.sponsors.length) {
      console.log('requested index is out of range')
      return 'requested index out of range'
    }
    else {
      console.log('requested index is in range')
      if (state.sponsors[index] == 'undefined') {
        console.log("requested index doesn't exist")
        return 'id doesnt exist'
      }
      else {
        try {
          let id = state.sponsors[index]
          return id
        }
        catch {
          console.log('posts/getsponsorid reading state.posts[index].postid failed')
          // Handle next post missing case
          return false
        }
      }
    }

    // implement not found
  }
}

// https://www.youtube.com/watch?v=NS0io3Z75GI
export const actions = {
  async loadPosts({ commit } : {commit:any}) {
    return SponsorService.getPosts().then(response => {
      commit('SET_POSTS', response.data.posts)
    })
  }
}

/*mutations: {
        SET_RESPONSE(p) {
            state.posts = p;
        }
    },
    actions: {
        async getIP ({ commit }) {
            const posts = await this.$axios.$get('localhost:9000/fi/posts')
            commit('SET_RESPONSE', posts)
        }
    },
    getters: {}
}


/*
export const state: {

    actions: {
      async getIP ({ commit }) {
        const ip = await this.$axios.$get('http://icanhazip.com')
        commit('SET_IP', ip)
      }
    }
}
*/
