import RoundService from "~/services/RoundService"

export const state = () => ({
    antellround: {},
})

export const mutations = {
    SET_ROUND(state: any, responsemenu:any) {
        state.antellround = responsemenu
    }
}

export const actions = {
    async loadMenus({ commit } : {commit:any}) {
      return RoundService.getMenu().then(response => {
        commit('SET_ROUND', response.data)
      })
    }
  }