import DocumentService from "~/services/PostsService"

export const state = () => ({
  documents: [] as any[],
  error: null as any
})

export type DocumentState = ReturnType<typeof state>

export const mutations = {
  SET_SPONSORS(state: any, responsedocuments: any) {
    state.documents = responsedocuments
  },
}

export const getters = {
  // Searches for correct array index by id and returns that
  getDocumentIndex: (state: any) => (id: any) => {
    let i
    for (i = 0; i <= state.documents.length; i++) {
      console.log("beforeerror?: " + state.documents)
      try {
        if (state.documents[i].url == id) {
          return i
        }
      } catch {
        console.log("get documentindex failed")
        return 0
      }

      console.log("aftererror?")
    }
    // Implement not found error here!!
  },
  getDocumentId: (state: any) => (index: any) => {
    if (index <= -1 || index > state.documents.length) {
      console.log('requested index is out of range')
      return 'requested index out of range'
    }
    else {
      console.log('requested index is in range')
      if (state.documents[index] == 'undefined') {
        console.log("requested index doesn't exist")
        return 'id doesnt exist'
      }
      else {
        try {
          let id = state.documents[index]
          return id
        }
        catch {
          console.log('posts/getdocumentid reading state.posts[index].postid failed')
          // Handle next post missing case
          return false
        }
      }
    }

    // implement not found
  }
}

// https://www.youtube.com/watch?v=NS0io3Z75GI
export const actions = {
  async loadPosts({ commit } : {commit:any}) {
    return DocumentService.getPosts().then(response => {
      commit('SET_POSTS', response.data.posts)
    })
  }
}