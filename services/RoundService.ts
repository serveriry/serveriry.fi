import axios from 'axios'

const apiClient = axios.create({
  baseURL: `https://www.antell.fi/round/wp-json/antell/v1`,
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

export default {
  getMenu() {
    // returns a promise
    return apiClient.get('/lunch')
  }/*,
  getEvent(id) {
    return apiClient.get('/events/' + id)
  }*/
}