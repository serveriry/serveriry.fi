import axios from 'axios'

const apiClient = axios.create({
  baseURL: `localhost:1337`,
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

export default {
  getPosts() {
    // returns a promise
    return apiClient.get('/posts?_sort=published_at:desc')
  }/*,
  getEvent(id) {
    return apiClient.get('/events/' + id)
  }*/
}