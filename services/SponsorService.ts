import axios from 'axios'

const apiClient = axios.create({
  baseURL: `localhost:1337`,
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

export default {
  getSponsors() {
    // returns a promise
    return apiClient.get('/sponsors?_sort=published_at:desc')
  }/*,
  getEvent(id) {
    return apiClient.get('/events/' + id)
  }*/
}