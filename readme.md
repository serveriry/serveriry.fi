# Serveri ry website

_Instructions are mainly for system administrators at Serveri ry and this is why it includes backend configurations.
Feel free to try things out, but **do not impersonate Serveri ry**_

This project consists of two main parts

1. Frontend written in Nuxt.js
2. Strapi backend/api (not included in this git repo anymore)

(RSS generator will be a third component and it will be created later)

Backend configuration publicly available, because it was too difficult to remove all keys from Strapi source. Strapi source can be found at https://github.com/strapi/strapi . However if you have access to Serveri ry server you can grab the running back-sqlite directory from there if for example needed for further development. If you want to further develop the website and need custom api features, contact admin. If you need api for other than Serveri ry use, just contact us before doing so. You will likely receive a permission but for load balancing reasons use case information is welcome. RSS feed can be used wherever you wish.

At the time of writing, backend documentation can be found here: https://strapi.io/documentation

Stylesheets were developed for dart-sass, but seems like nuxt defaults to node-sass. All used styles are converted node-sass compatible so dart-sass shouldn't be required, but there may appear dart-sass references.

Most styles are scoped to component only and some classes are imported from assets/scss

For both backend and frontend it is recommended to use PM2 in cluster mode. ecosystem file is included

<hr>

# Configuration

### Short setup steps for frontend
set api host in store/settings.ts before compile

1. Configure settings.ts.template to settings.ts (located in ./store/ directory)
2. Create nuxt.config.js from template (listen ports etc.)
3. Do the same for the process manager file called ecosystem.config
4. npm install (you can check everything is ok with npm run dev)
5. npm run build 
6. pm2 start ecosystem.config.js

### Short setup for backend

1. npm install
2. NODE_ENV=production npm run build (you can check the server with NODE_ENV=production npm start)
3. pm2 start ecosystem.config.js --env production 

   if this fails then reinstall node modules

pm2 commands are: pm2 [start|restart|stop|delete] ecosystem.config.js -[optional-variable]

<hr>

# Updates to frontend

1. Make sure changes will compile
2. git pull
3. npm install (if packages have changed)
4. (on beta change ecosystem.config app name to 'ServettiBeta' and listen port back to default 3000)
5. npm run build
6. pm2 restart ecosystem.config.js
   
   done

Some changes may require a cache purge from CDN (cloudflare)

## API Updates to backend
**BACKUP**
**BACKUP**
**BACKUP**

When doing version updates, refer to strapi.io documentation. Remember to look at migration guides. Instagram migration on version 3.4.x not done, we don't need.

1. Stop server (pm2 stop ecosystem.config.js)
2. start with development mode on (npm run develop)
3. do changes using the admin panel
99. run npm run build -- --clean
4. pm2 restart ecosystem.config.js --env production

if this fails try reinstalling node modules

   donee!
