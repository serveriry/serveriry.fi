Some functions written in go and compiled to wasm in order to improve performance

when doing changes make sure to copy recompiled binaries to ../static/wasm if not done already

wasm_exec.js is a javascript "glue" which allows running the go binaries and it is sourced from 
go version go1.16.8 linux/amd64

binaries can be compiled from this dir using go command:
GOOS=js GOARCH=wasm go build -o ../static/wasm/main.wasm main.go