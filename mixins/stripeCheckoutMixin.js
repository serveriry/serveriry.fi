export default {
    methods: {
      checkout: function(event) {
        this.stripe
          .redirectToCheckout({
            lineItems: [{ price: this.sku, quantity: 1 }],
            mode: 'payment',
            successUrl: this.successUrl,
            cancelUrl: this.cancelUrl
          })
          .then(function(result) {
            if (result.error) {
              var displayError = document.getElementById('error-message')
              displayError.textContent = result.error.message
            }
          })
      }
    },
    mounted() {
      this.stripe = Stripe(this.$store.state.settings.stripekey)
    }
  }